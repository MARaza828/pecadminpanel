﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace AdminPanelPecApi.ApiRequestHandler
{
    public class APIAccessHandler : DelegatingHandler
    {
        private const string Authorization = "Pec-1234";
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpResponceMessage, CancellationToken cancellationToken)
        {
            bool ValidKey = false;
            IEnumerable<string> requestHeaders;
            var checkApiKeyExists = httpResponceMessage.Headers.TryGetValues("APIKey", out requestHeaders);
            if (checkApiKeyExists)
            {
                if (requestHeaders.FirstOrDefault().Equals(Authorization))
                {
                    ValidKey = true;
                }
            }
            if (!ValidKey)
            {
                return httpResponceMessage.CreateResponse(HttpStatusCode.Forbidden, "Connection Unsucessful.");

            }
            var response = await base.SendAsync(httpResponceMessage, cancellationToken);
            return response;
        }
    }
}
