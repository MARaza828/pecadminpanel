﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanelPecApi.Models
{
    public class FaqViewModel
    {
        public long ID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string Statement { get; set; }
        public string Answer { get; set; }
        public int Category { get; set; }
        public string CreatedBy { get; set; }
        public long LastUpdatedBy { get; set; }
        public System.DateTime LastUpdatedAt { get; set; }
        public long RecordStatus { get; set; }
    }
}