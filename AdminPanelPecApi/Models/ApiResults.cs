﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace AdminPanelPecApi.Models
{
    public class ApiResults
    {
        string url = "http://localhost:49950//api";
        #region FAQ Api
        public IEnumerable<FAQ> GetFaqList(IEnumerable<FAQ> faq)
        {

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");

            client.BaseAddress = new Uri(url + "/FAQ");
            var ApiObj = client.GetAsync("FAQ");
            ApiObj.Wait();
            var readDataFromApi = ApiObj.Result;
            if (readDataFromApi.IsSuccessStatusCode)
            {
                var displayData = readDataFromApi.Content.ReadAsAsync<IList<FAQ>>();
                displayData.Wait();
                faq = displayData.Result;

            }
            return faq;

        }
        public bool InsertFaqRecord(FAQ newfaq)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/FAQ");
            var insertFaq = client.PostAsJsonAsync<FAQ>("FAQ", newfaq);
            insertFaq.Wait();
            var saveRecord = insertFaq.Result;
            if (saveRecord.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }
        public FAQ GetFaqDetails(long id, FAQ faq)
        {

            FAQ faqData = null;
            HttpClient client = new HttpClient();

            client.BaseAddress = client.BaseAddress = new Uri(url + "/FAQ");
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            var ApiObj = client.GetAsync("FAQ?id=" + id.ToString());
            ApiObj.Wait();
            var ReadDataFromApi = ApiObj.Result;
            if (ReadDataFromApi.IsSuccessStatusCode)
            {
                var displayData = ReadDataFromApi.Content.ReadAsAsync<FAQ>();
                displayData.Wait();
                faqData = displayData.Result;

            }
            return faqData;

        }
        public FAQ GetFaqEditDetail(long id)
        {
            FAQ faqData = null;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/FAQ");
            var ApiObj = client.GetAsync("FAQ?id=" + id.ToString());
            ApiObj.Wait();
            var ReadDataFromApi = ApiObj.Result;
            if (ReadDataFromApi.IsSuccessStatusCode)
            {
                var displayData = ReadDataFromApi.Content.ReadAsAsync<FAQ>();
                displayData.Wait();
                faqData = displayData.Result;

            }
            return faqData;
        }

        public bool SaveFaqEditedRecord(long id, FAQ editfaq)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/FAQ");
            var insertFaq = client.PutAsJsonAsync<FAQ>("FAQ?id=" + id.ToString(), editfaq);
            insertFaq.Wait();
            var saveRecord = insertFaq.Result;
            if (saveRecord.IsSuccessStatusCode)
            {
                return true;
            }
            else { return false; }
        }

        public bool DeleteFaqRecord(long id)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/FAQ");
            var DeleteFaq = client.DeleteAsync("FAQ?id=" + id.ToString());
            DeleteFaq.Wait();
            var DeleteData = DeleteFaq.Result;
            if (DeleteData.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion

        #region Complaints Api
        public IEnumerable<Complaint> GetComplaintList()
        {
            IEnumerable<Complaint> complaints = null;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");

            client.BaseAddress = new Uri(url + "/Complaint");
            var ApiObj = client.GetAsync("Complaint");
            ApiObj.Wait();
            var readDataFromApi = ApiObj.Result;
            if (readDataFromApi.IsSuccessStatusCode)
            {
                var displayData = readDataFromApi.Content.ReadAsAsync<IList<Complaint>>();
                displayData.Wait();
                complaints = displayData.Result;

            }
            return complaints;

        }

        public Complaint GetComplaintDetails(long id)
        {

            Complaint complaintsData = null;
            HttpClient client = new HttpClient();

            client.BaseAddress = client.BaseAddress = new Uri(url + "/Complaint");
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            var ApiObj = client.GetAsync("Complaint?id=" + id.ToString());
            ApiObj.Wait();
            var ReadDataFromApi = ApiObj.Result;
            if (ReadDataFromApi.IsSuccessStatusCode)
            {
                var displayData = ReadDataFromApi.Content.ReadAsAsync<Complaint>();
                displayData.Wait();
                complaintsData = displayData.Result;

            }
            return complaintsData;

        }

        public bool InsertComplaintRecord(Complaint newComplaint)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/Complaint");
            var insertComplaint = client.PostAsJsonAsync<Complaint>("Complaint", newComplaint);
            insertComplaint.Wait();
            var saveRecord = insertComplaint.Result;
            if (saveRecord.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        public IEnumerable<Complaint> SearchRecord(string TrackingID)
        {
            IEnumerable<Complaint> complaints = null;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");

            client.BaseAddress = new Uri("http://localhost:49950//api/Complaint");
            var ApiObj = client.GetAsync("Complaint?search=" + TrackingID);
            ApiObj.Wait();
            var readDataFromApi = ApiObj.Result;
            if (readDataFromApi.IsSuccessStatusCode)
            {
                var displayData = readDataFromApi.Content.ReadAsAsync<IList<Complaint>>();
                displayData.Wait();
                complaints = displayData.Result;

            }
            return complaints;
        }

        public async Task<bool> SendEmail(EmailComplaintsViewModel emailData)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/Complaint");
            HttpResponseMessage ForwardComplaint = await client.PostAsJsonAsync<EmailComplaintsViewModel>("Complaint", emailData);
            //ForwardComplaint.Wait();
            var saveRecord = ForwardComplaint;
            if (saveRecord.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region NewsEvents Api

        public IEnumerable<NewsEvent> GetNewsEventList()
        {
            IEnumerable<NewsEvent> NewsEventData = null;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/NewsEvent");
            var ApiObj = client.GetAsync("NewsEvent");
            ApiObj.Wait();
            var readDataFromApi = ApiObj.Result;
            if (readDataFromApi.IsSuccessStatusCode)
            {
                var displayData = readDataFromApi.Content.ReadAsAsync<IList<NewsEvent>>();
                displayData.Wait();
                NewsEventData = displayData.Result;
            }
            return NewsEventData;
        }
        public bool InsertNewsEventRecord(NewsEvent NewsEventData)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/NewsEvent");
            var insertNewsEvent = client.PostAsJsonAsync<NewsEvent>("NewsEvent", NewsEventData);
            insertNewsEvent.Wait();
            var saveRecord = insertNewsEvent.Result;
            if (saveRecord.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        public NewsEvent GetNewsEventDetails(long id)
        {

            NewsEvent newsEventData = null;
            HttpClient client = new HttpClient();

            client.BaseAddress = client.BaseAddress = new Uri(url + "/NewsEvent");
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            var ApiObj = client.GetAsync("NewsEvent?id=" + id.ToString());
            ApiObj.Wait();
            var ReadDataFromApi = ApiObj.Result;
            if (ReadDataFromApi.IsSuccessStatusCode)
            {
                var displayData = ReadDataFromApi.Content.ReadAsAsync<NewsEvent>();
                displayData.Wait();
                newsEventData = displayData.Result;

            }
            return newsEventData;

        }

        public NewsEvent GetNewsEventEditDetail(long id)
        {
            NewsEvent newsEventData = null;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/NewsEvent");
            var ApiObj = client.GetAsync("NewsEvent?id=" + id.ToString());
            ApiObj.Wait();
            var ReadDataFromApi = ApiObj.Result;
            if (ReadDataFromApi.IsSuccessStatusCode)
            {
                var displayData = ReadDataFromApi.Content.ReadAsAsync<NewsEvent>();
                displayData.Wait();
                newsEventData = displayData.Result;

            }
            return newsEventData;
        }

        public bool SaveNewsEventEditedRecord(long id, NewsEvent editNewsEvent)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/NewsEvent");
            var insertNewsEvent = client.PutAsJsonAsync<NewsEvent>("NewsEvent?id=" + id.ToString(), editNewsEvent);
            insertNewsEvent.Wait();
            var saveRecord = insertNewsEvent.Result;
            if (saveRecord.IsSuccessStatusCode)
            {
                return true;
            }
            else { return false; }
        }

        public bool DeleteNewsEventRecord(long id)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/NewsEvent");
            var DeleteNewsEvent = client.DeleteAsync("NewsEvent?id=" + id.ToString());
            DeleteNewsEvent.Wait();
            var DeleteData = DeleteNewsEvent.Result;
            if (DeleteData.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Feedback Api

        public IEnumerable<Feedback> GetFeedbackList()
        {
            IEnumerable<Feedback> feedback = null;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/Feedback");
            var ApiObj = client.GetAsync("Feedback");
            ApiObj.Wait();
            var readDataFromApi = ApiObj.Result;
            if (readDataFromApi.IsSuccessStatusCode)
            {
                var displayData = readDataFromApi.Content.ReadAsAsync<IList<Feedback>>();
                displayData.Wait();
                feedback = displayData.Result;

            }
            return feedback;

        }

        public Feedback GetFeedbackDetails(long id)
        {

            Feedback feedbackData = null;
            HttpClient client = new HttpClient();

            client.BaseAddress = client.BaseAddress = new Uri(url + "/Feedback");
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            var ApiObj = client.GetAsync("Feedback?id=" + id.ToString());
            ApiObj.Wait();
            var ReadDataFromApi = ApiObj.Result;
            if (ReadDataFromApi.IsSuccessStatusCode)
            {
                var displayData = ReadDataFromApi.Content.ReadAsAsync<Feedback>();
                displayData.Wait();
                feedbackData = displayData.Result;

            }
            return feedbackData;

        }

        public bool InsertFeedbackRecord(Feedback Newfeedback)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("APIKey", "Pec-1234");
            client.BaseAddress = new Uri(url + "/Feedback");
            var insertFaq = client.PostAsJsonAsync<Feedback>("Feedback", Newfeedback);
            insertFaq.Wait();
            var saveRecord = insertFaq.Result;
            if (saveRecord.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}