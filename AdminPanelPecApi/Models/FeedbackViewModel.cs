﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanelPecApi.Models
{
    public class FeedbackViewModel
    {
        public long ID { get; set; }
        public long ComplaintID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string Message { get; set; }
        public string FeedbackFileUrl { get; set; }
        public string CreatedBy { get; set; }
        public long LastUpdatedBy { get; set; }
        public System.DateTime LastUpdatedAt { get; set; }
        public long RecordStatus { get; set; }
    }
}