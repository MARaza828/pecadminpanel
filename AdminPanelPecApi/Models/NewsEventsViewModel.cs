﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanelPecApi.Models
{
    public class NewsEventsViewModel
    {
        public long ID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<long> StatusID { get; set; }
        public string ImageUrl { get; set; }
        public long CreatedBy { get; set; }
        public long LastUpdatedBy { get; set; }
        public System.DateTime LastUpdatedAt { get; set; }
        public long RecordStatus { get; set; }
    }
}