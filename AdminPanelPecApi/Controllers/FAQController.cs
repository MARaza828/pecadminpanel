﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AdminPanelPecApi.Models;

namespace AdminPanelPecApi.Controllers
{
    public class FAQController : ApiController
    {
        private ComplaintsPortalEntities db = new ComplaintsPortalEntities();

        // GET: api/FAQsApi
        public IQueryable<FAQ> GetFAQs()
        {
                return db.FAQs;
        }

        // GET: api/FAQsApi/5
        [ResponseType(typeof(FAQ))]
        public IHttpActionResult GetFAQ(long id)
        {
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return NotFound();
            }

            return Ok(fAQ);
        }

        // PUT: api/FAQsApi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFAQ(long id, FAQ fAQ)
        {

            fAQ.CreatedDate = DateTime.Now.Date;
            fAQ.CreatedBy = 1;
            fAQ.LastUpdatedAt = DateTime.Now.Date;
            fAQ.LastUpdatedBy = 1;
            fAQ.Category = 1;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fAQ.ID)
            {
                return BadRequest();
            }

            db.Entry(fAQ).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FAQExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FAQsApi
        [ResponseType(typeof(FAQ))]
        public IHttpActionResult PostFAQ(FAQ fAQ)
        {
            try
            {
                fAQ.CreatedDate = DateTime.Now.Date;
                fAQ.CreatedBy = 1;
                fAQ.LastUpdatedAt = DateTime.Now.Date;
                fAQ.LastUpdatedBy = 1;
                fAQ.Category = 1;

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.FAQs.Add(fAQ);
                db.SaveChanges();
                return StatusCode(HttpStatusCode.OK);
                //return CreatedAtRoute("DefaultApi", new { id = fAQ.ID }, fAQ);
            }
            catch (Exception ex)
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/FAQsApi/5
        [ResponseType(typeof(FAQ))]
        public IHttpActionResult DeleteFAQ(long id)
        {
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return NotFound();
            }

            db.FAQs.Remove(fAQ);
            db.SaveChanges();

            return Ok(fAQ);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FAQExists(long id)
        {
            return db.FAQs.Count(e => e.ID == id) > 0;
        }
    }
}