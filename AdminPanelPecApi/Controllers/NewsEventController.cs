﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AdminPanelPecApi.Models;
using System.IO;
using System.Web;

namespace AdminPanelPecApi.Controllers
{
    public class NewsEventController : ApiController
    {
        private ComplaintsPortalEntities db = new ComplaintsPortalEntities();

        // GET: api/NewsEventsApi
        public IQueryable<NewsEvent> GetNewsEvents()
        {
            return db.NewsEvents;
        }

        // GET: api/NewsEventsApi/5
        [ResponseType(typeof(NewsEvent))]
        public IHttpActionResult GetNewsEvent(long id)
        {
            NewsEvent newsEvent = db.NewsEvents.Find(id);
            if (newsEvent == null)
            {
                return NotFound();
            }

            return Ok(newsEvent);
        }

        // PUT: api/NewsEventsApi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNewsEvent(long id, NewsEvent newsEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != newsEvent.ID)
            {
                return BadRequest();
            }

            db.Entry(newsEvent).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NewsEventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/NewsEventsApi
        [ResponseType(typeof(NewsEvent))]
        public IHttpActionResult PostNewsEvent(NewsEvent newsEvent)
        {
            newsEvent.CreatedBy = 1;
            newsEvent.CreatedDate = DateTime.Now.Date;
            newsEvent.LastUpdatedAt = DateTime.Now.Date;
            newsEvent.LastUpdatedBy = 1;
            newsEvent.RecordStatus = 1;
            newsEvent.StatusID = 1;
           
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //if (Request.Files.Count > 0)
            //{
            //    var file = Request.Files[0];
            //    if (file != null && file.ContentLength > 0)
            //    {
            //        string filename = Path.GetFileName(file.FileName);
            //        string path = Path.Combine(Server.MapPath("~/uploads/"), filename);
            //        file.SaveAs(path);
            //        newsEvent.ImageUrl = filename;
            //    }
            //}
                //int uploadedFiles = 0;


                string path = HttpContext.Current.Server.MapPath("~/uploads/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            //
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var fileupload = HttpContext.Current.Request.Files[0];
                if (fileupload != null)
                {
                    var saveImage = Path.Combine(HttpContext.Current.Server.MapPath("~/uploads/"), fileupload.FileName);
                    fileupload.SaveAs(saveImage);
                    newsEvent.ImageUrl = saveImage.ToString();
                }
            }

            //HttpFileCollection httpFilesRequest = HttpContext.Current.Request.Files;
            //for (int i = 0; i <= 1; i++)
            //{
            //    HttpPostedFile postedFile = httpFilesRequest[i];
            //    if (postedFile.ContentLength > 0)
            //    {
            //        if (!File.Exists(path + Path.GetFileName(postedFile.FileName)))
            //        {
            //            postedFile.SaveAs(path + Path.GetFileName(postedFile.FileName));
            //            uploadedFiles = uploadedFiles + 1;
            //            newsEvent.ImageUrl = postedFile.FileName;
            //        }
            //    }
            //}
            //if (uploadedFiles > 0)
            //{
            //    db.NewsEvents.Add(newsEvent);
            //    db.SaveChanges();
            //}
            //return CreatedAtRoute("DefaultApi", new { id = newsEvent.ID }, newsEvent);

            //HttpResponseMessage result = null;
            //var httpRequest = HttpContext.Current.Request;
            //if (httpRequest.Files.Count > 0)
            //{
            //    var file = httpRequest.Files[0];
            //    if (file != null && file.ContentLength > 0)
            //    {
            //        string filename = file.FileName;
            //        string path = HttpContext.Current.Server.MapPath("~/uploads/" + filename);
            //        file.SaveAs(path);
            //        newsEvent.ImageUrl = filename;
            //    }


            //}
            db.NewsEvents.Add(newsEvent);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = newsEvent.ID }, newsEvent);

            //return CreatedAtRoute("DefaultApi", new { id = newsEvent.ID }, newsEvent);
        }

        // DELETE: api/NewsEventsApi/5
        [ResponseType(typeof(NewsEvent))]
        public IHttpActionResult DeleteNewsEvent(long id)
        {
            NewsEvent newsEvent = db.NewsEvents.Find(id);
            if (newsEvent == null)
            {
                return NotFound();
            }

            db.NewsEvents.Remove(newsEvent);
            db.SaveChanges();

            return Ok(newsEvent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NewsEventExists(long id)
        {
            return db.NewsEvents.Count(e => e.ID == id) > 0;
        }
    }
}