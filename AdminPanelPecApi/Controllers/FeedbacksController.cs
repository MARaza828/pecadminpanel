﻿using AdminPanelPecApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace AdminPanelPecApi.Controllers
{
    public class FeedbacksController : Controller
    {
        ApiResults apiResult = new ApiResults();
        // GET: Feedbacks
        public ActionResult Index()
        {
            
            return View(apiResult.GetFeedbackList());
        }

        public ActionResult Details(long id)
        {
           
            return View(apiResult.GetFeedbackDetails(id));
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Create(Feedback Newfeedback)
        {
            int response;
            if (apiResult.InsertFeedbackRecord(Newfeedback))
            {
                return Json(response = 1, JsonRequestBehavior.AllowGet);
            }
            return Json(response = 0, JsonRequestBehavior.AllowGet);
        }
    }
}