﻿using AdminPanelPecApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanelPecApi.Controllers
{
    public class FAQsController : Controller
    {
        ApiResults apiResult = new ApiResults();
        // GET: FAQs
        public ActionResult Index()
        {

            IEnumerable<FAQ> faqData = null;

            return View(apiResult.GetFaqList(faqData));
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        //public ActionResult Create(FAQ newfaq)
        //{
        //    if (apiResult.InsertFaqRecord(newfaq))
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    return View("Create");
        //}
        public JsonResult Create(FAQ newfaq)
        {
            int response;
            if (apiResult.InsertFaqRecord(newfaq))
            {
                return Json(response = 1, JsonRequestBehavior.AllowGet);
            }
            return Json(response = 0, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long id)
        {
            FAQ faqData = null;
            return View(apiResult.GetFaqDetails(id, faqData));
        }
        public ActionResult Edit(long id)
        {
            return View(apiResult.GetFaqEditDetail(id));
        }
        [HttpPost]
        //public ActionResult Edit(long id, FAQ editfaq)
        //{
        //    //HttpClient client = new HttpClient();
        //    //client.BaseAddress = new Uri("http://localhost:49942/api/FAQsApi");
        //    //var insertFaq = client.PutAsJsonAsync<FAQ>("FAQsApi?id=" + id.ToString(), editfaq);
        //    //insertFaq.Wait();
        //    //var saveRecord = insertFaq.Result;
        //    if (apiResult.SaveFaqEditedRecord(id,editfaq))
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        ViewBag.message = "Record Not Found.";
        //    }
        //    return View(editfaq);
        //}

        public JsonResult Edit(long id, FAQ editfaq)
        {
            //HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("http://localhost:49942/api/FAQsApi");
            //var insertFaq = client.PutAsJsonAsync<FAQ>("FAQsApi?id=" + id.ToString(), editfaq);
            //insertFaq.Wait();
            //var saveRecord = insertFaq.Result;
            int response;
            if (apiResult.SaveFaqEditedRecord(id, editfaq))
            {
                return Json(response = 1, JsonRequestBehavior.AllowGet);
            }
            return Json(response = 0, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult Delete(long id)
        //{
        //    //HttpClient client = new HttpClient();
        //    //client.BaseAddress = new Uri("http://localhost:49942/api/FAQsApi");
        //    //var DeleteFaq = client.DeleteAsync("FAQsApi?id=" + id.ToString());
        //    //DeleteFaq.Wait();
        //    //var DeleteData = DeleteFaq.Result;
        //    if (apiResult.DeleteFaqRecord(id))
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    return View("Index");
        //}



        public JsonResult Delete(long id)
        {
            int response;
            //HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("http://localhost:49942/api/FAQsApi");
            //var DeleteFaq = client.DeleteAsync("FAQsApi?id=" + id.ToString());
            //DeleteFaq.Wait();
            //var DeleteData = DeleteFaq.Result;
            if (apiResult.DeleteFaqRecord(id))
            {
                return Json(response = 1, JsonRequestBehavior.AllowGet);
            }
            return Json(response = 0, JsonRequestBehavior.AllowGet);
        }
    }
}