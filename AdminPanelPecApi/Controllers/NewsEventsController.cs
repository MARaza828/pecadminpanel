﻿using AdminPanelPecApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace AdminPanelPecApi.Controllers
{
    public class NewsEventsController : Controller
    {
        ApiResults apiResult = new ApiResults();
        // GET: NewsEvents
        public ActionResult Index()
        {
            return View(apiResult.GetNewsEventList());
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Create(NewsEvent NewsEventData)
        {
            int response;
            if (apiResult.InsertNewsEventRecord(NewsEventData))
            {
                return Json(response = 1, JsonRequestBehavior.AllowGet);
            }
            return Json(response = 0, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long id)
        {
           
            return View(apiResult.GetNewsEventDetails(id));
        }

        public ActionResult Edit(long id)
        {
           
            return View(apiResult.GetNewsEventEditDetail(id));
        }
        [HttpPost]
        public JsonResult Edit(long id, NewsEvent editNewsEvent)
        {
            
                int response;
            if (apiResult.SaveNewsEventEditedRecord(id,editNewsEvent))
            {
                return Json(response = 1, JsonRequestBehavior.AllowGet);
            }
            return Json(response = 0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(long id)
        {
            int response;
            if (apiResult.DeleteNewsEventRecord(id))
            {
                return Json(response = 1, JsonRequestBehavior.AllowGet);
            }
            return Json(response = 0, JsonRequestBehavior.AllowGet);
        }
    }
}