﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AdminPanelPecApi.Models;
using static AdminPanelPecApi.Enumerations.Enumeration;
using System.Threading.Tasks;
using System.Net.Mail;

namespace AdminPanelPecApi.Controllers
{
    public class ComplaintController : ApiController
    {
        private ComplaintsPortalEntities db = new ComplaintsPortalEntities();

        // GET: api/Complaint
        public IQueryable<Complaint> GetComplaints()
        {
            return db.Complaints;
        }

        // GET: api/Complaint/5
        [ResponseType(typeof(Complaint))]
        public IHttpActionResult GetComplaint(long id)
        {
            Complaint complaint = db.Complaints.Find(id);
            if (complaint == null)
            {
                return NotFound();
            }

            return Ok(complaint);
        }

        // PUT: api/Complaint/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutComplaint(long id, Complaint complaint)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != complaint.ID)
            {
                return BadRequest();
            }

            db.Entry(complaint).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComplaintExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
       
       
        // POST: api/Complaint
        [ResponseType(typeof(Complaint))]
        public IHttpActionResult PostComplaint(Complaint complaint)
        {
            complaint.CreatedDate = DateTime.Now.Date; //Current Date Time
            complaint.StatusID = (long)ComplaintStatus.Pending;
            complaint.LastUpdatedAt = DateTime.Now.Date; //Current Date Time
            complaint.LastUpdatedBy = 1; //ID from Session
            complaint.RecordStatus = 1; // Record Status
            complaint.CreatedBy = 1;
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Complaints.Add(complaint);


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ComplaintExists(complaint.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = complaint.ID }, complaint);
        }

        // DELETE: api/Complaint/5
        [ResponseType(typeof(Complaint))]
        public IHttpActionResult DeleteComplaint(long id)
        {
            Complaint complaint = db.Complaints.Find(id);
            if (complaint == null)
            {
                return NotFound();
            }

            db.Complaints.Remove(complaint);
            db.SaveChanges();

            return Ok(complaint);
        }

   
        public IHttpActionResult GetTrackRecord(string search)
        {
            //try
            //{
                var complaint = db.Complaints.Where(x => x.TrackingID == search).ToList();
            if (complaint != null)
            {
                return Ok(complaint);
            }
            else
            {
                return NotFound();
            }
                //}
            //catch (Exception ex)
            //{
            //}

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ComplaintExists(long id)
        {
            return db.Complaints.Count(e => e.ID == id) > 0;
        }
    }
}