﻿using AdminPanelPecApi.Enumerations;
using AdminPanelPecApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace AdminPanelPecApi.Controllers
{
    public class ComplaintsController : Controller
    {
        ComplaintsPortalEntities db = new ComplaintsPortalEntities();
        ApiResults apiResult = new ApiResults();
        // GET: Complaints
        public ActionResult Index()
        {
            //Complaint model = new Complaint();

            //IEnumerable<Complaint> ComplaintsData = null;
            // model.Compaints = 

            return View(apiResult.GetComplaintList());
        }
        public ActionResult Details(long id)
        {
            //Complaint complaintData = null;
            return View(apiResult.GetComplaintDetails(id));
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Create(Complaint newComplaint)
        {
            int response;
            if (apiResult.InsertComplaintRecord(newComplaint))
            {
                return Json(response = 1, JsonRequestBehavior.AllowGet);
            }
            return Json(response = 0, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ComplaintStatus(string TrackingID)
        {

            return View(apiResult.SearchRecord(TrackingID));
        }
        
        public JsonResult SendEmail(EmailComplaintsViewModel emailData)
        {
            try
            {
                //var file = ConvertThisPageToPdf();
                var Subject = emailData.Subject;
                var Body = emailData.AdminComments + emailData.Message;
                var UserEmail = "alb191.akhaliq@gmail.com";
                var Password = "adi@03349681828";
                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(UserEmail, emailData.ToEmail))
                {
                    mm.Subject = Subject;
                    string body = EmailBody(emailData);
                    //file Attachment If Required
                    //string[] code = body.Split('>');
                    //string codedigit = code[1];
                    //codedigit = codedigit.Substring(0, 4);
                    // string FilePath = link;
                    //Attachment attachment = new Attachment(ResolveUrl(FilePath));
                    //mm.Attachments.Add(attachment);
                    //Attachment at = new Attachment("C:\\Users\\AlberuniTech\\Downloads\\sample-file.pdf");
                    //mm.Attachments.Add(at);
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.UseDefaultCredentials = false;
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("alb191.akhaliq@gmail.com", "adi@03349681828");
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    Complaint complaintStatus = db.Complaints.Find(emailData.ComplaintID);
                    complaintStatus.StatusID = (long)Enumeration.ComplaintStatus.Open;
                    complaintStatus.LastUpdatedAt = DateTime.Now.Date;
                    complaintStatus.LastUpdatedBy = 1;
                    complaintStatus.CreatedBy = 1;
                    db.Entry(complaintStatus).State = EntityState.Modified;
                    //db.SaveChanges();

                    Comment commentsData = new Comment();
                    commentsData.CreatedDate = DateTime.Now.Date;
                    commentsData.ComplaintID = emailData.ComplaintID;
                    commentsData.Comments = emailData.AdminComments;
                    commentsData.CreatedBy = 1;
                    commentsData.LastUpdatedAt = DateTime.Now.Date;
                    commentsData.LastUpdatedBy = 1;
                    commentsData.RecordStatus = 1;
                    db.Comments.Add(commentsData);
                    db.SaveChanges();

                    // System.IO.File.Delete(@"C:\\Users\\AlberuniTech\\Downloads\\sample-file.pdf");
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        private string EmailBody(EmailComplaintsViewModel emailData)

        {

            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(Server.MapPath("~/DesignEmail/EmailTemplate.html")))

            {
                
                body = reader.ReadToEnd();

            }
            body = body.Replace("{AdminComments}", emailData.AdminComments);
            body = body.Replace("{TrackingID}", emailData.TrackingID);
            body = body.Replace("{Subject}", emailData.Subject); //replacing the required things
            body = body.Replace(" {Message}", emailData.Message);


            return body;

        }

        public ActionResult GetEmailRecord(long id)
        {
            Complaint complaint = db.Complaints.Find(id);
            return View(complaint);
        }
    }
}