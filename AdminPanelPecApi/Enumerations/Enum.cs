﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanelPecApi.Enumerations
{
    public class Enumeration
    {
        public enum ComplaintStatus
        {
            Pending = 0,
            Open = 1,
            Close=2
        }
        public enum ComplaintCommentType
        {
            User = 0,
            Admin=1
        }
    }
}